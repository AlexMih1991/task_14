#include <iostream>
#include <string>

int main()
{
	std::cout << "Enter any phrase: ";
	std::string phrase;
	std::getline(std::cin, phrase);

	std::cout << "\n" << "Your phrase - " << phrase << "\n" << "\n";

	std::cout << "Lenght phrase = " << phrase.length() << "\n";
	std::cout << "First character = " << phrase.front() << "\n";
	std::cout << "Last character = " << phrase.back() << "\n";


}